# Type Inference algorithm.


1. Make use of the unification algorithm in the last lab to arrive at
   a type inference algorithm for Hindley-Milner typed lambda
   calculus.
